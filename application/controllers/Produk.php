<?php

require APPPATH . '/libraries/REST_Controller.php';

class produk extends REST_Controller
{

    public function __construct($config = "rest")
    {
        parent::__construct($config);
        $this->load->model("Produk_model", "produk");
    }

    public function index_get()
    {
        $id = $this->get('id');
        $cari = $this->get('cari');

        if ($cari != "") {
            $produk = $this->produk->cari($cari)->result();
        } else if($id == "") {
            $produk = $this->produk->getData(null)->result();
        } else {
            $produk = $this->produk->getData($id)->result();
        }

        $this->response($produk);       
    }

    public function index_put()
    {
        $gambar = $this->put('gambar');
        $nama = $this->put('nama');
        $tahun = $this->put('tahun');
        $stok = $this->put('stok');
        $harga = $this->put('harga');
        $kategori = $this->put('kategori');
        $id = $this->put('id');
        $data = array(
            'gambar_produk' => $gambar,
            'nama_produk' => $nama,
            'tahun_produk' => $tahun,
            'stok' => $stok,
            'harga' => $harga,
            'kategori_produk' => $kategori
        );
        $update = $this->produk->update('tbl_produk', $data, 'id_produk', $id);

        if ($update) {
            $this->response(array('status' => 'succes', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }   
    }

    public function index_post()
    {
        $gambar = $this->post('gambar');
        $nama = $this->post('nama');
        $tahun = $this->post('tahun');
        $stok = $this->post('stok');
        $harga = $this->post('harga');
        $kategori = $this->post('kategori');
        $data = array(
            'gambar_produk' => $gambar,
            'nama_produk' => $nama,
            'tahun_produk' => $tahun,
            'stok' => $stok,
            'harga' => $harga,
            'kategori_produk' => $kategori
        );
        $insert = $this->produk->insert($data);

        if ($insert) {
            $this->response(array('status' => 'succes', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_delete()
    {
        $id = $this->delete('id');
        $delete = $this->produk->delete('tbl_produk', 'id_produk', $id);

        if ($delete) {
            $this->response(array('status' => 'succes', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}