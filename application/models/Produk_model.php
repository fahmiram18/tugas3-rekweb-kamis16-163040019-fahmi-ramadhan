<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function getData($id = null){
        $this->db->select('*');
        $this->db->from('tbl_produk');
        if ($id == null) {
            $this->db->order_by('id_produk', 'asc');
        } else {
            $this->db->where('id_produk', $id);
        }
        
        return $this->db->get();
    }

    public function cari($cari)
    {
        $this->db->select('*');
        $this->db->from('tbl_produk');
        $this->db->like('nama_produk', $cari);
        $this->db->or_like('tahun_produk', $cari);
        $this->db->or_like('kategori_produk', $cari);
        return $this->db->get();
    }

    public function insert($data){
       $this->db->insert('tbl_produk', $data);
       return $this->db->affected_rows();
    }

    public function update($table, $data, $par, $var) {
        $this->db->update($table, $data, array($par => $var));
        return $this->db->affected_rows();
    }
    
    public function delete($table, $par, $var){
       $this->db->where($par, $var);
       $this->db->delete($table);
       return $this->db->affected_rows();
    }

}

/* End of file Produk_model.php */
/* Location: ./application/models/Produk_model.php */   